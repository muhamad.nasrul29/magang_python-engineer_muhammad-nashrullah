--
-- Create model Genre
--
CREATE TABLE `libraryapp_genre` (`id` bigint AUTO_INCREMENT NOT NULL PRIMARY KEY, `genre` varchar(50) NOT NULL);
--
-- Create model Roles
--
CREATE TABLE `libraryapp_roles` (`id` bigint AUTO_INCREMENT NOT NULL PRIMARY KEY, `role` varchar(8) NULL);
--
-- Create model User
--
CREATE TABLE `libraryapp_user` (`id` bigint AUTO_INCREMENT NOT NULL PRIMARY KEY, `nama` varchar(100) NOT NULL, `email` varchar(254) NOT NULL UNIQUE, `no_telpon` varchar(16) NOT NULL, `alamat` varchar(150) NOT NULL, `password` varchar(1024) NOT NULL, `roles_id` bigint NOT NULL);
--
-- Create model Books
--
CREATE TABLE `libraryapp_books` (`id` bigint AUTO_INCREMENT NOT NULL PRIMARY KEY, `judul_buku` varchar(200) NOT NULL, `deskripsi` longtext NOT NULL, `gambar` varchar(100) NOT NULL, `harga` integer UNSIGNED NOT NULL CHECK (`harga` >= 0), `status` bool NOT NULL, `publish` varchar(150) NOT NULL, `genre_id` bigint NOT NULL, `pemilik_id` bigint NOT NULL, `peminjam_id` bigint NULL);
ALTER TABLE `libraryapp_user` ADD CONSTRAINT `libraryapp_user_roles_id_9b563ede_fk_libraryapp_roles_id` FOREIGN KEY (`roles_id`) REFERENCES `libraryapp_roles` (`id`);
ALTER TABLE `libraryapp_books` ADD CONSTRAINT `libraryapp_books_genre_id_3b19f894_fk_libraryapp_genre_id` FOREIGN KEY (`genre_id`) REFERENCES `libraryapp_genre` (`id`);
ALTER TABLE `libraryapp_books` ADD CONSTRAINT `libraryapp_books_pemilik_id_8aea44b9_fk_libraryapp_user_id` FOREIGN KEY (`pemilik_id`) REFERENCES `libraryapp_user` (`id`);
ALTER TABLE `libraryapp_books` ADD CONSTRAINT `libraryapp_books_peminjam_id_cbbcc9eb_fk_libraryapp_user_id` FOREIGN KEY (`peminjam_id`) REFERENCES `libraryapp_user` (`id`);
