from django import forms
from .models import User, Books

class RegisterForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['nama', 'email', 'password', 'no_telpon', 'alamat', 'roles']
        widgets = {
            'password': forms.PasswordInput(),
        }

class LoginForm(forms.Form):
    email = forms.EmailField(label="Email")
    password = forms.CharField(label="Password", max_length=1024, widget=forms.PasswordInput())

class BookForm(forms.ModelForm):
    class Meta:
        model = Books
        fields = ['judul_buku', 'gambar', 'deskripsi', 'genre', 'publish', 'harga']

