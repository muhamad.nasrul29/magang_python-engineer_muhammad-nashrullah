from django.db import models
from django.db.models.base import Model
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager

# Create your models here.

class Roles(models.Model):
    role = models.CharField(max_length=8, null=True)
    def __str__(self):
        return self.role

class UserManager(BaseUserManager):
    def create_user(self, email, password, nama, roles, no_telpon=None, alamat=None):
        roles = roles if type(roles) == Roles else Roles.objects.get(id=roles)
        required = {'email': email==None, 
                    'password': password==None, 
                    'nama': nama==None,
                    'roles': roles==None}
        for field, status in required.items():
            if status:
                raise ValueError(f'{field} is required')
        user = self.model(email=self.normalize_email(email), nama=nama, roles=roles, no_telpon=no_telpon, alamat=alamat)
        user.set_password(password)
        user.save()
        return user

class User(AbstractBaseUser):
    nama = models.CharField("Nama",max_length=100)
    email = models.EmailField("Email", unique=True)
    no_telpon = models.CharField("Nomor Telpon",max_length=16, null=True, blank=True)
    alamat = models.CharField("Alamat", max_length=150, null=True, blank=True)
    password = models.CharField("Password", max_length=1024)
    roles = models.ForeignKey(Roles, on_delete=models.CASCADE, verbose_name="Roles")
    is_active = models.BooleanField(default=True)
    objects = UserManager()
    USERNAME_FIELD = 'email'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = ['nama', 'roles']
    is_active = 'is_active'

class Genre(models.Model):
    genre = models.CharField(max_length=50)
    def __str__(self) -> str:
        return self.genre

class Books(models.Model):
    genre = models.ForeignKey(Genre, on_delete=models.PROTECT)
    judul_buku = models.CharField(max_length=200)
    deskripsi = models.TextField()
    gambar = models.ImageField(upload_to='photo/')
    harga = models.PositiveIntegerField()
    pemilik = models.ForeignKey(User, on_delete=models.PROTECT)
    peminjam = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True, related_name='peminjam')
    status = models.BooleanField(default=False)
    publish = models.CharField(max_length=150)